from org.csstudio.display.builder.runtime.script import PVUtil
import os
import subprocess
from os.path import expanduser
from time import sleep

start           = PVUtil.getInt(pvs[7])    #start_offset_estimation

if start == 1:

    pv_test = PVUtil.createPV("loc://test_pv",0)
    pv_test.write("Sweeping/Measuring")
    PVUtil.releasePV(pv_test)

    power_lvl_start = PVUtil.getDouble(pvs[0]) #sig_gen_start
    power_lvl_end   = PVUtil.getDouble(pvs[1]) #sig_gen_finish
    PSG             = PVUtil.getString(pvs[2]) #$(P_SG)
    RSG             = PVUtil.getString(pvs[3]) #$(R_SG)
    #PPM             = PVUtil.getString(pvs[4]) #$(P_PM)
    # Temporary hack
    P             = 'rfcalib:'
    RPM             = 'RFS-NRP-01:'
    RSG             = 'RFS-SMC-01:'
    att_desc        = PVUtil.getString(pvs[6]) #attenuation_description

    siggen = P + RSG
    powmet = P + RPM

##    pv_test = PVUtil.createPV("loc://test_pv",0)
##    pv_test.write(powmet)
##    PVUtil.releasePV(pv_test)

    # Set continuous measurement of power meter OFF
    temp = PVUtil.createPV(powmet + "setRun",5000)
    temp.write(0)
    PVUtil.releasePV(temp)
    sleep(0.5)

    # Send trigger OFF Power meter
    temp = PVUtil.createPV(powmet + "Value_DBM.SCAN",5000)
    temp.write(0)
    PVUtil.releasePV(temp)

    temp = PVUtil.createPV(powmet + "offset_switch",5000)
    temp.write(0)
    PVUtil.releasePV(temp)

    os.chdir("/opt/opi-test/Calibration/scripts")
    #return_val = os.system("./estimate_power_meter_offset.py %f %f %s %s %s" % (power_lvl_start, power_lvl_end, siggen, powmet, att_desc))
    return_val = subprocess.Popen('./estimate_power_meter_offset.py %f %f %s %s %s' % (power_lvl_start, power_lvl_end, siggen, powmet, att_desc), stdout = subprocess.PIPE, stderr = subprocess.PIPE , shell = True)
    return_val = str(return_val.communicate(return_val.stdout))
    print(return_val)
    print(return_val[2:9])
    return_val = float( return_val[2:9])

    if (att_desc == '"Signal cable"'):
        temp = PVUtil.createPV("loc://cable_attenuation",0)
        temp.write(return_val)
        PVUtil.releasePV(temp)

        # return to start parameter to 0
        pv_startsp = PVUtil.createPV("loc://start_cable_offset_estimation", 5000)
        pv_startsp.write(0)
        PVUtil.releasePV(pv_startsp)
    elif (att_desc == '"Directional coupler"'):
        temp = PVUtil.createPV("loc://coupler_attenuation",0)
        temp.write(return_val)
        PVUtil.releasePV(temp)

        # return to start parameter to 0
        pv_startsp = PVUtil.createPV("loc://start_Dircoup_offset_estimation", 5000)
        pv_startsp.write(0)
        PVUtil.releasePV(pv_startsp)

    pv_test = PVUtil.createPV("loc://test_pv",0)
    pv_test.write("Sweep complete")
    PVUtil.releasePV(pv_test)
