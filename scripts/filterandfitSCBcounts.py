#!/usr/bin/env python3

import os
import sys
import numpy as np
from epics import PV, caget, caput
import time
#import matplotlib.pyplot as plt

os.environ["EPICS_CA_ADDR_LIST"] = "172.16.110.25 172.16.110.15 172.16.110.13 172.16.110.33 172.16.110.32 172.16.110.89"

system_name  = sys.argv[1]
SCBfilename = sys.argv[2]
KW           = int(float(sys.argv[3]))
DC_coupling  = float(sys.argv[4])
cable_number = sys.argv[5]
cable_att    = float(sys.argv[6])
filter_att   = float(sys.argv[7])

filepath = "../Measurements/"+ system_name + "/SCB/" +SCBfilename + "_raw.csv"
data = np.genfromtxt(filepath,delimiter=',')

Y = data[:,0]
C = data[:,1]

C_limit = C[len(C)-1] - 100

Y = Y[C<C_limit]
C = C[C<C_limit]
#plt.plot(C,Y,marker=11)
C = C + 32768

Y = np.reshape(Y,(len(Y),1))
C = np.reshape(C,(len(C),1))

Csq = np.square(C)

SCBsycnt = np.arange(0,65536,1)
SCBsycnt = np.reshape(SCBsycnt,(len(SCBsycnt),1))

del_Y   = np.diff(Y,axis=0)
del_Csq = np.diff(Csq,axis=0)
del_C   = np.diff(C,axis=0)

A = np.hstack((del_Csq,del_C))
coeff = np.matmul( np.linalg.inv(  np.matmul( np.transpose(A),A ))  , np.matmul( np.transpose(A), del_Y) )

noise = Y - np.matmul( np.hstack((Csq,C)), coeff)
eps = np.mean(noise)

Yest = np.matmul( np.hstack((np.square(SCBsycnt),SCBsycnt)), coeff) + eps
SCBsycnt = SCBsycnt - 32768

#plt.plot(SCBsycnt,Yest)
#plt.xlabel('Raw SCB ADC')
#plt.ylabel('Power (EGU)')
#plt.savefig("../Measurements/"+ system_name + "/SCB/" + SCBfilename + '.png')
#plt.close()

scbfile = open("../Measurements/"+ system_name + "/SCB/" + SCBfilename + ".csv","w")
if KW == 1:
    scbfile.write("# KWatt" + ',' + " raw" +'\n')
else:
    scbfile.write("# Watt" + ',' + " raw" +'\n')

for cnt in range(len(SCBsycnt)):
    scbfile.write(str(Yest[cnt][0]) + ',' + str(SCBsycnt[cnt][0]) +'\n')
scbfile.write('# \n')
scbfile.write('# \n')
scbfile.write('# \n')
scbfile.write('# \n')
scbfile.write('# -----------------------------------------------------------\n')
scbfile.write("# Wave-guide Directional coupler attenuation is " + str(DC_coupling) + "dB" +'\n')
scbfile.write("# Cable number from Wave-guide Directional coupler to patch panel  is " + cable_number +'\n')
scbfile.write("# Cable attenuation is " + str(cable_att) + "dB" +'\n')
scbfile.write("# Filter and directional coupler attenuation is " + str(filter_att) + "dB" +'\n')
scbfile.write('# -----------------------------------------------------------\n')
scbfile.close()
