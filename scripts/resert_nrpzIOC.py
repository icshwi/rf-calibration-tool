#!/usr/bin/env python3

import os
import sys
import numpy as np
from epics import PV, caget, caput
import time

os.environ["EPICS_CA_ADDR_LIST"] = "172.16.110.25 172.16.110.15 172.16.110.13 172.16.110.33 172.16.110.32 172.16.110.89"
caput('rfcalib-rs_nrpz-01:SysReset',1)
