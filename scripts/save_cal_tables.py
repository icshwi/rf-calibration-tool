from org.csstudio.display.builder.runtime.script import PVUtil
import os
import subprocess
from os.path import expanduser
from time import sleep
import math

save_cal_table = PVUtil.getInt(pvs[13])

if save_cal_table == 1:
    SCBmeas             = PVUtil.getDoubleArray(pvs[0])
    LLRFmeas            = PVUtil.getDoubleArray(pvs[1])
    PWMTmeas            = PVUtil.getDoubleArray(pvs[2])
    cal_llrf            = PVUtil.getInt(pvs[3])           # cal_llrf
    cal_scb             = PVUtil.getInt(pvs[4])           # cal_scb
    SCBfilename         = PVUtil.getString(pvs[5])        # loc://SCB_filename
    LLRFfilename        = PVUtil.getString(pvs[6])        # loc://LLRF_filename
    KW                  = PVUtil.getDouble(pvs[7])        # loc://KW
    DC_coupling         = PVUtil.getDouble(pvs[8])
    cable_att           = PVUtil.getDouble(pvs[9])
    cable_number        = PVUtil.getString(pvs[10])
    filter_att          = PVUtil.getDouble(pvs[11])
    system_name         = PVUtil.getString(pvs[12])

    os.chdir("/opt/opi-test/RF_Calibration_Tool/scripts")
    
    temp = PVUtil.createPV("loc://message",5000)
    temp.write('Trying to save tables')
    PVUtil.releasePV(temp)
    
    if cal_llrf == 1:
        temp = PVUtil.createPV("loc://message",5000)
        temp.write('Trying to save LLRF tables')
        PVUtil.releasePV(temp)
        return_val = subprocess.Popen('./filterandfitLLRFcounts.py %s %s %s %s %s %s %s' % (system_name,LLRFfilename,str(KW),str(DC_coupling),cable_number,str(cable_att),str(filter_att)), stdout = subprocess.PIPE, stderr = subprocess.PIPE , shell = True)
        return_val = str(return_val.communicate(return_val.stdout))
        temp = PVUtil.createPV("loc://message",5000)
        temp.write(return_val)
        PVUtil.releasePV(temp)
        temp = PVUtil.createPV("loc://message",5000)
        temp.write('LLRF calibration table saved.')
        PVUtil.releasePV(temp)
        sleep(2)

    if cal_scb == 1:
        temp = PVUtil.createPV("loc://message",5000)
        temp.write('Trying to save SCB tables')
        PVUtil.releasePV(temp)
        return_val = subprocess.Popen('./filterandfitSCBcounts.py %s %s %s %s %s %s %s' % (system_name,SCBfilename,str(KW),str(DC_coupling),cable_number,str(cable_att),str(filter_att)), stdout = subprocess.PIPE, stderr = subprocess.PIPE , shell = True)
        return_val = str(return_val.communicate(return_val.stdout))
        temp = PVUtil.createPV("loc://message",5000)
        temp.write(return_val)
        PVUtil.releasePV(temp)
        temp = PVUtil.createPV("loc://message",5000)
        temp.write('SCB calibration table saved.')
        PVUtil.releasePV(temp)
        sleep(2)
