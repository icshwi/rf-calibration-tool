from org.csstudio.display.builder.runtime.script import PVUtil
import os
import subprocess
from os.path import expanduser
from time import sleep
import math

def calc_median(data):
    data.sort()
    if (len(data)%2 == 0):
        data_median = (data[int(len(data)/2)-1]+data[int(len(data)/2)])/2
    else:
        data_median = data[int((len(data)+1)/2)-1]

    return data_median


def calc_mean(data):
    return sum(data)/len(data)

def write_message(message):
    temp = PVUtil.createPV("loc://message",5000)
    temp.write(message)
    PVUtil.releasePV(temp)

def read_pv(script4read, pv_name):
    return_val = subprocess.Popen(script4read +' %s' % (powmet + pv_name), stdout = subprocess.PIPE, stderr = subprocess.PIPE , shell = True)
    return_val = str(return_val.communicate(return_val.stdout))
    return_val = return_val.lstrip("""('""")
    return_val = return_val.rstrip("""', '')""")
    return_val = return_val.replace("n",'')
    return_val = return_val[0:(len(return_val)-1)]
    return return_val
    

def reset_RS_NRPZ(powmet):
    temp = PVUtil.createPV(powmet + "SensorReset-S",5000)
    temp.write(1)
    PVUtil.releasePV(temp)
    sleep(0.5)
    # read correction frequency back to check that reset has happened
    corr_freq = 0
    while corr_freq != 50000000:
        corr_freq = read_pv('./read_power_meter.py', "CorrFrequency-RBV")
        corr_freq = float(corr_freq)
        write_message('Waiting on power meter: ' + powmet + ' reset.')
        sleep(0.5)
    write_message('Power meter: ' + powmet + ' reset successful.')
    temp = PVUtil.createPV(powmet + "SensorReset-S",5000)
    temp.write(0)
    PVUtil.releasePV(temp)
    reset_done = 0
    while reset_done == 0:
        sleep(0.5)
        ret_val = read_pv('./read_power_meter.py',"SensorReset-R")
        if int(ret_val) == 0:
            reset_done = 1
        else:
            write_message(' ')
    # Write correction values back
    corr_freq = read_pv('./read_power_meter.py', "CorrFrequency-S")
    corr_freq = float(corr_freq)
    temp = PVUtil.createPV(powmet + "CorrFrequency-S",5000)
    temp.write(corr_freq)
    PVUtil.releasePV(temp)
    corr_freq_set = 0
    while corr_freq_set == 0:
        sleep(0.5)
        ret_val = read_pv('./read_power_meter.py',"CorrFrequency-RBV")
        write_message('Frequency correction ret_val: ' + str(ret_val))
        if float(ret_val) != 50000000.0:
            corr_freq_set = 1
            write_message('Power meter frequency correction done')
        else:
            write_message('Waiting for power meter frequency correction')
    write_message('Correction freq: ' + str(corr_freq))
    sleep(0.5)
    # set trigger source to immediate
    temp = PVUtil.createPV(powmet + "TrigSource-S",5000)
    temp.write(3)
    PVUtil.releasePV(temp)
    trig_set = 0
    while trig_set == 0:
        sleep(0.5)
        ret_val = read_pv('./read_power_meter.py',"TrigSource-RBV")
        if int(ret_val) == 3:
            trig_set = 1
            write_message('Power meter trigger set')
        else:
            write_message('Waiting for power meter trigger to set')
    offset = read_pv('./read_power_meter.py', "CorrOffset-S")
    offset = float(offset)
    temp = PVUtil.createPV(powmet + "CorrOffset-S",5000)
    temp.write(offset)
    PVUtil.releasePV(temp)
    corr_off_set = 0
    while corr_off_set == 0:
        sleep(0.5)
        off_set_rbv = read_pv('./read_power_meter.py',"CorrOffset-RBV")
        if float(off_set_rbv) != 0.0:
            corr_off_set = 1
            write_message('Power meter offset correction set')
        else:
            write_message('Waiting for power meter offset correction to set')
    write_message('Correction offset: ' + str(offset))
    sleep(0.5)
    temp = PVUtil.createPV(powmet + "CorrOffsetEn-S",5000)
    temp.write(1)
    PVUtil.releasePV(temp)
    offset_en = 0
    while offset_en == 0:
        sleep(0.5)
        ret_val = read_pv('./read_power_meter.py',"CorrOffsetEn-RBV")
        if int(ret_val) == 1:
            offset_en = 1
            write_message('Power meter offset enabled')
        else:
            write_message('Waiting for power meter offset enable')


def initialize_powermeter(powmet):
    write_message('Initializing power meter: ' + powmet)
    if powmet != 'rfcalib:RS_NRPZ-01-Ch0:':
        # Set continuous measurement of power meter OFF
        temp = PVUtil.createPV(powmet + "setRun",5000)
        temp.write(0)
        PVUtil.releasePV(temp)
        sleep(0.5)
        # Send measure OFF to Power meter
        temp = PVUtil.createPV(powmet + "Value_DBM.SCAN",5000)
        temp.write(0)
        PVUtil.releasePV(temp)
    else:
        # Send trigger OFF Power meter
        temp = PVUtil.createPV(powmet + "SensorAcquire-S",5000)
        temp.write(0)
        PVUtil.releasePV(temp)
        sleep(0.5)
        # Send sensor reset
        reset_RS_NRPZ(powmet)


start_cal                      = PVUtil.getInt(pvs[13]) #start_cal
if start_cal == 1:
    cal_scb                    = PVUtil.getInt(pvs[0]) #cal_scb
    cal_llrf                   = PVUtil.getInt(pvs[1]) #cal_llrf
    powmet                     = PVUtil.getString(pvs[2]) #powermeter
    siggen                     = PVUtil.getString(pvs[3]) #signalgenerator
    sig_gen_start              = PVUtil.getDouble(pvs[4])
    sig_gen_finish             = PVUtil.getDouble(pvs[5])
    DC_couplingcoeff           = PVUtil.getDouble(pvs[6])
    timimg_prefix              = PVUtil.getString(pvs[7])
    LLRF_PV                    = PVUtil.getString(pvs[8])
    SCB_PV                     = PVUtil.getString(pvs[9])
    measinKW                   = PVUtil.getDouble(pvs[10]) #Normally we measure in KWatts
    cable_att                  = PVUtil.getDouble(pvs[11])
    filter_att                 = PVUtil.getDouble(pvs[12])
    if (cal_scb == 1):
        number_calibration_points   = 40
        num_shrt_calibration_points = 25
        number_samples              = 50
    else:
        number_calibration_points   = 40
        num_shrt_calibration_points = 25
        number_samples              = 25
    os.chdir("/opt/opi-test/Calibration/scripts")
    if cal_scb == 1:
        pv_scb = SCB_PV                                           # RFQ-010:RFS-PAmp-110:PwrFwd:RawAvg
        cal_ch = pv_scb.replace(":RawAvg","")                     # RFQ-010:RFS-PAmp-110:PwrFwd
        
    if cal_llrf == 1:
        pv_llrf = LLRF_PV                                         # RFQ-010:RFS-DIG-102:AI0-SMonAvg-Mag
        llrf_prefix = pv_llrf.replace("-SMonAvg-Mag","")          # RFQ-010:RFS-DIG-102:AI0
        temp = PVUtil.createPV(llrf_prefix + "-SMonAvgPos",5000)  # RFQ-010:RFS-DIG-102:AI0-SMonAvgPos
        temp.write(8255)
        PVUtil.releasePV(temp)
        temp = PVUtil.createPV(llrf_prefix + "-SMonAvgWid",5000) # RFQ-010:RFS-DIG-102:AI0-SMonAvgWid
        temp.write(4000)
        PVUtil.releasePV(temp)
    # Allowable % around median
    error = 0.075
    lwred = 1-(error/100)
    hgred = 1+(error/100)
    # Signal generator power output sweep levels
    #Write initial value to signal generator
    temp = PVUtil.createPV(siggen + "GeneralPPwr-SP",5000)
    temp.write(sig_gen_start)
    PVUtil.releasePV(temp)
    sleep(0.5)
    # Convert from dBm to Watts
    sig_gen_start  = 10**((sig_gen_start - 30)/10)
    # Convert from dBm to Watts
    sig_gen_finish = 10**((sig_gen_finish - 30)/10)
    # Calculate power meter power level step size in Watts
    sig_gen_step = (sig_gen_finish-sig_gen_start)/(number_calibration_points-1)
    # Switch signal generator RF ON
    temp = PVUtil.createPV(siggen + "GeneralRF-Sel",5000)
    temp.write(1)
    PVUtil.releasePV(temp)
    sleep(2)
    PWMTmeas = []
    LLRFmeas = []
    SCBmeas  = []
    if cal_scb == 1:
        #Enable calibration mode for SCB channel
        temp = PVUtil.createPV(cal_ch +":EnCalibMode",5000) # RFQ-010:RFS-PAmp-110:PwrFwd:EnCalibMode
        temp.write(1)
        PVUtil.releasePV(temp)
    initialize_powermeter(powmet)
    PMreading   = []
    SCBreading  = []
    LLRFreading = []
    filter_SCBreading  = []
    filter_LLRFreading = []
    filter_PMreading   = []
    cnt = 0
    while (cnt < num_shrt_calibration_points):
        PMreading[:]   = []
        SCBreading[:]  = []
        LLRFreading[:] = []
        # Set signal generator power level
        sig_gen_level = sig_gen_start + cnt*(sig_gen_step/250)
        # Convert from Watts to dBm
        sig_gen_in = 10*math.log10(sig_gen_level) + 30
        # Set signal generator power level
        temp = PVUtil.createPV(siggen + "GeneralPPwr-SP",5000)
        temp.write(sig_gen_in)
        PVUtil.releasePV(temp)
        sleep(0.5)
        for meas_cnt in range(number_samples):
            # Read Power meter
            if powmet != 'rfcalib:RS_NRPZ-01-Ch0:':
                # Send trigger ON to Power meter
                temp = PVUtil.createPV(powmet + "Value_DBM.SCAN",5000)
                temp.write(9)
                PVUtil.releasePV(temp)
                sleep(0.5)
                # Send trigger OFF Power meter
                temp = PVUtil.createPV(powmet + "Value_DBM.SCAN",5000)
                temp.write(0)
                PVUtil.releasePV(temp)
                sleep(0.5)
            else:
                # Send trigger ON to Power meter
                temp = PVUtil.createPV(powmet + "SensorAcquire-S",5000)
                temp.write(1)
                PVUtil.releasePV(temp)
                trig_en = 0
                while trig_en == 0:
                    sleep(0.5)
                    ret_val = read_pv('./read_power_meter.py',"SensorAcquire-R")
                    ret_val = int(ret_val)
                    if int(ret_val) == 1:
                        trig_en = 1
                        write_message('Power meter successfully triggered ON')
                    else:
                        write_message('Waiting for power meter to trigger ON')
                # Send trigger OFF Power meter
                temp = PVUtil.createPV(powmet +  "SensorAcquire-S",5000)
                temp.write(0)
                PVUtil.releasePV(temp)
                trig_en = 0
                while trig_en == 0:
                    sleep(0.5)
                    ret_val = read_pv('./read_power_meter.py',"SensorAcquire-R")
                    ret_val = int(ret_val)
                    if int(ret_val) == 0:
                        trig_en = 1
                        write_message('Power meter successfully triggered OFF')
                    else:
                        write_message('Waiting for power meter to trigger OFF')
            # Read Power meter reading in dBm
            data_read = 0
            while data_read == 0:
                try:
                    if powmet == 'rfcalib:RS_NRPZ-01-Ch0:':
                        pow_met_read = read_pv('./read_power_meter.py', "MeasValueDbm-R")
                    else:
                        pow_met_read = read_pv('./read_power_meter.py', "Value_DBM")
                    pow_met_read = float(pow_met_read)
                    data_read  = 1
                    write_message('Power meter data read successful')
                except:
                    data_read = 0
            # Correct power meter reading for dirctional coupler
            pow_met_read = pow_met_read + DC_couplingcoeff + cable_att + filter_att
            # Convert corrected power meter reading into Watts
            pow_met_read = 10**((pow_met_read - 30)/10)
            # Convert corrected power meter reading to KWatts if applicable
            if measinKW == 1:
                pow_met_read = pow_met_read/1000
            PMreading.append(pow_met_read)
            # Save power meter
            if cal_scb == 1:
                #Read Signal Conditioning Board PV value
                data_read = 0
                while data_read == 0:
                    try:
                        return_val = subprocess.Popen('./read_scb_pv.py %s' % (pv_scb), stdout = subprocess.PIPE, stderr = subprocess.PIPE , shell = True)
                        return_val = str(return_val.communicate(return_val.stdout))
                        return_val = return_val.lstrip("""('""")
                        return_val = return_val.rstrip("""', '')""")
                        return_val = return_val.replace("n",'')
                        return_val = return_val[0:(len(return_val)-1)]
                        return_val = float( return_val)
                        data_read  = 1
                    except:
                        data_read = 0
                SCBreading.append(return_val+32768)
            if cal_llrf == 1:
                data_read = 0
                while data_read == 0:
                    try:
                        return_val = subprocess.Popen('./read_llrf_pv.py %s' % (pv_llrf), stdout = subprocess.PIPE, stderr = subprocess.PIPE , shell = True)
                        return_val = str(return_val.communicate(return_val.stdout))
                        return_val = return_val.lstrip("""('""")
                        return_val = return_val.rstrip("""', '')""")
                        return_val = return_val.replace("n",'')
                        return_val = return_val[0:(len(return_val)-1)]
                        return_val = float(return_val)
                        data_read  = 1
                    except:
                        data_read = 0
                LLRFreading.append(return_val)
        if powmet == 'rfcalib:RS_NRPZ-01-Ch0:':
            # Send sensor reset
            reset_RS_NRPZ(powmet)
        # Send data for plot
        temp = PVUtil.createPV("loc://PM_meas_obs",5000)
        temp.write(PMreading)
        PVUtil.releasePV(temp)
        sleep(0.5)
        if cal_llrf == 1:
            # Send data for plot
            temp = PVUtil.createPV("loc://LLRF_meas_obs",5000)
            temp.write(LLRFreading)
            PVUtil.releasePV(temp)
            sleep(0.1)
        if cal_scb == 1:
            # Send data for plot
            temp = PVUtil.createPV("loc://SCB_meas_obs",5000)
            temp.write(SCBreading)
            PVUtil.releasePV(temp)
            sleep(0.1)
        filter_SCBreading[:]  = []
        filter_LLRFreading[:] = []
        filter_PMreading[:]   = []
        if cal_scb == 1:
            # Filter indices which are within allowable % of median
            SCB_median = calc_median(SCBreading)
            PM_median  = calc_median(PMreading)
            # Filter list with obtained indices
            for cmp_cnt in range(len(SCBreading)):
                if (SCBreading[cmp_cnt]>(lwred*SCB_median))&(SCBreading[cmp_cnt]<(hgred*SCB_median))&(PMreading[cmp_cnt]>(lwred*PM_median))&(PMreading[cmp_cnt]<(hgred*PM_median)):
                    filter_SCBreading.append(SCBreading[cmp_cnt]-32768)
                    filter_PMreading.append(PMreading[cmp_cnt])
        if cal_llrf == 1:
            # Filter indices which are within allowable % of median
            LLRF_median = calc_median(LLRFreading)
            PM_median  = calc_median(PMreading)
            # Filter list with obtained indices
            for cmp_cnt in range(len(LLRFreading)):
                if (LLRFreading[cmp_cnt]>(lwred*LLRF_median))&(LLRFreading[cmp_cnt]<(hgred*LLRF_median))&(PMreading[cmp_cnt]>(lwred*PM_median))&(PMreading[cmp_cnt]<(hgred*PM_median)):
                    filter_LLRFreading.append(LLRFreading[cmp_cnt])
                    filter_PMreading.append(PMreading[cmp_cnt])
        # Send data for plot
        temp = PVUtil.createPV("loc://PM_meas_obs",5000)
        temp.write(filter_PMreading)
        PVUtil.releasePV(temp)
        sleep(0.1)
        if cal_llrf == 1:
            # Send data for plot
            temp = PVUtil.createPV("loc://LLRF_meas_obs",5000)
            temp.write(filter_LLRFreading)
            PVUtil.releasePV(temp)
            sleep(0.1)
        if cal_scb == 1:
            # Send data for plot
            temp = PVUtil.createPV("loc://SCB_meas_obs",5000)
            temp.write(filter_SCBreading)
            PVUtil.releasePV(temp)
            sleep(0.1)
        # Compute mean of filtered list and save in measurement list
        try:
            PWMTmeas.append(calc_mean(filter_PMreading))
            if cal_scb == 1:
                SCBmeas.append(calc_mean(filter_SCBreading))
            
            if cal_llrf == 1:
                LLRFmeas.append(calc_mean(filter_LLRFreading))
            cnt = cnt + 1
        except:
            cnt = cnt - 1

    cnt = 1
    while (cnt<number_calibration_points):
        PMreading[:]   = []
        SCBreading[:]  = []
        LLRFreading[:] = []
        # Set signal generator power level
        sig_gen_level = sig_gen_start + cnt*(sig_gen_step)
        # Convert from Watts to dBm
        sig_gen_in = 10*math.log10(sig_gen_level) + 30
        # Set signal generator power level
        temp = PVUtil.createPV(siggen + "GeneralPPwr-SP",5000)
        temp.write(sig_gen_in)
        PVUtil.releasePV(temp)
        sleep(0.5)
        for meas_cnt in range(number_samples):
            # Read Power meter
            if powmet != 'rfcalib:RS_NRPZ-01-Ch0:':
                # Send trigger ON to Power meter
                temp = PVUtil.createPV(powmet + "Value_DBM.SCAN",5000)
                temp.write(9)
                PVUtil.releasePV(temp)
                sleep(0.5)
                # Send trigger OFF Power meter
                temp = PVUtil.createPV(powmet + "Value_DBM.SCAN",5000)
                temp.write(0)
                PVUtil.releasePV(temp)
                sleep(0.5)
            else:
                # Send trigger ON to Power meter
                temp = PVUtil.createPV(powmet + "SensorAcquire-S",5000)
                temp.write(1)
                PVUtil.releasePV(temp)
                trig_en = 0
                while trig_en == 0:
                    sleep(0.1)
                    ret_val = read_pv('./read_power_meter.py',"SensorAcquire-R")
                    ret_val = int(ret_val)
                    if int(ret_val) == 1:
                        trig_en = 1
                        write_message('Power meter successfully triggered ON')
                    else:
                        write_message('Waiting for power meter to trigger ON')
                # Send trigger OFF Power meter
                temp = PVUtil.createPV(powmet +  "SensorAcquire-S",5000)
                temp.write(0)
                PVUtil.releasePV(temp)
                trig_en = 0
                while trig_en == 0:
                    sleep(0.1)
                    ret_val = read_pv('./read_power_meter.py',"SensorAcquire-R")
                    ret_val = int(ret_val)
                    if int(ret_val) == 0:
                        trig_en = 1
                        write_message('Power meter successfully triggered OFF')
                    else:
                        write_message('Waiting for power meter to trigger OFF')
            # Read Power meter reading in dBm
            data_read = 0
            while data_read == 0:
                try:
                    if powmet == 'rfcalib:RS_NRPZ-01-Ch0:':
                        pow_met_read = read_pv('./read_power_meter.py', "MeasValueDbm-R")
                    else:
                        pow_met_read = read_pv('./read_power_meter.py', "Value_DBM")
                    pow_met_read = float(pow_met_read)
                    data_read  = 1
                    write_message('Power meter data read successful')
                except:
                    data_read = 0
            # Correct power meter reading for dirctional coupler
            pow_met_read = pow_met_read + DC_couplingcoeff + cable_att + filter_att
            # Convert corrected power meter reading into Watts
            pow_met_read = 10**((pow_met_read - 30)/10)
            # Convert corrected power meter reading to KWatts if applicable
            if measinKW == 1:
                pow_met_read = pow_met_read/1000
            PMreading.append(pow_met_read)
            # Save power meter
            if cal_scb == 1:
                #Read Signal Conditioning Board PV value
                data_read = 0
                while data_read == 0:
                    try:
                        return_val = subprocess.Popen('./read_scb_pv.py %s' % (pv_scb), stdout = subprocess.PIPE, stderr = subprocess.PIPE , shell = True)
                        return_val = str(return_val.communicate(return_val.stdout))
                        return_val = return_val.lstrip("""('""")
                        return_val = return_val.rstrip("""', '')""")
                        return_val = return_val.replace("n",'')
                        return_val = return_val[0:(len(return_val)-1)]
                        return_val = float( return_val)
                        data_read  = 1
                    except:
                        data_read = 0
                SCBreading.append(return_val+32768) # to force all values into positive range
            if cal_llrf == 1:
                data_read = 0
                while data_read == 0:
                    try:
                        return_val = subprocess.Popen('./read_llrf_pv.py %s' % (pv_llrf), stdout = subprocess.PIPE, stderr = subprocess.PIPE , shell = True)
                        return_val = str(return_val.communicate(return_val.stdout))
                        return_val = return_val.lstrip("""('""")
                        return_val = return_val.rstrip("""', '')""")
                        return_val = return_val.replace("n",'')
                        return_val = return_val[0:(len(return_val)-1)]
                        return_val = float(return_val)
                        data_read  = 1
                    except:
                        data_read = 0
                LLRFreading.append(return_val)
        if powmet == 'rfcalib:RS_NRPZ-01-Ch0:':
            # Send sensor reset
            reset_RS_NRPZ(powmet)
        # Send data for plot
        temp = PVUtil.createPV("loc://PM_meas_obs",5000)
        temp.write(PMreading)
        PVUtil.releasePV(temp)
        sleep(0.1)
        if cal_llrf == 1:
            # Send data for plot
            temp = PVUtil.createPV("loc://LLRF_meas_obs",5000)
            temp.write(LLRFreading)
            PVUtil.releasePV(temp)
            sleep(0.1)
        if cal_scb == 1:
            # Send data for plot
            temp = PVUtil.createPV("loc://SCB_meas_obs",5000)
            temp.write(SCBreading)
            PVUtil.releasePV(temp)
            sleep(0.1)
        filter_SCBreading[:]  = []
        filter_LLRFreading[:] = []
        filter_PMreading[:]   = []
        if cal_scb == 1:
            # Filter indices which are within allowable % of median
            SCB_median = calc_median(SCBreading)
            PM_median  = calc_median(PMreading)
            # Filter list with obtained indices
            for cmp_cnt in range(len(SCBreading)):
                if (SCBreading[cmp_cnt]>(lwred*SCB_median))&(SCBreading[cmp_cnt]<(hgred*SCB_median))&(PMreading[cmp_cnt]>(lwred*PM_median))&(PMreading[cmp_cnt]<(hgred*PM_median)):
                    filter_SCBreading.append(SCBreading[cmp_cnt]-32768) # going back to true range
                    filter_PMreading.append(PMreading[cmp_cnt])
        if cal_llrf == 1:
            # Filter indices which are within allowable % of median
            LLRF_median = calc_median(LLRFreading)
            PM_median  = calc_median(PMreading)
            # Filter list with obtained indices
            for cmp_cnt in range(len(LLRFreading)):
                if (LLRFreading[cmp_cnt]>(lwred*LLRF_median))&(LLRFreading[cmp_cnt]<(hgred*LLRF_median))&(PMreading[cmp_cnt]>(lwred*PM_median))&(PMreading[cmp_cnt]<(hgred*PM_median)):
                    filter_LLRFreading.append(LLRFreading[cmp_cnt])
                    filter_PMreading.append(PMreading[cmp_cnt])
        # Send data for plot
        temp = PVUtil.createPV("loc://PM_meas_obs",5000)
        temp.write(filter_PMreading)
        PVUtil.releasePV(temp)
        sleep(0.1)
        if cal_llrf == 1:
            # Send data for plot
            temp = PVUtil.createPV("loc://LLRF_meas_obs",5000)
            temp.write(filter_LLRFreading)
            PVUtil.releasePV(temp)
            sleep(0.1)
        if cal_scb == 1:
            # Send data for plot
            temp = PVUtil.createPV("loc://SCB_meas_obs",5000)
            temp.write(filter_SCBreading)
            PVUtil.releasePV(temp)
            sleep(0.1)
        # Compute mean of filtered list and save in measurement list
        try:
            PWMTmeas.append(calc_mean(filter_PMreading))
            if cal_scb == 1:
                SCBmeas.append(calc_mean(filter_SCBreading))
            if cal_llrf == 1:
                LLRFmeas.append(calc_mean(filter_LLRFreading))
            cnt = cnt + 1
        except:
            cnt = cnt - 1
        # Set signal generator power level
        sig_gen_level = sig_gen_level + sig_gen_step
        # Convert from Watts to dBm
        sig_gen_in = 10*math.log10(sig_gen_level) + 30

        # Set signal generator power level
        temp = PVUtil.createPV(siggen + "GeneralPPwr-SP",5000)
        temp.write(sig_gen_in)
        PVUtil.releasePV(temp)

    
    #print(PWMTmeas)
    #print(LLRFmeas)
    #print(SCBmeas)

    # Switch OFF signal generator
    temp = PVUtil.createPV(siggen + "GeneralRF-Sel",5000)
    temp.write(0)
    PVUtil.releasePV(temp)

    temp = PVUtil.createPV("loc://start_cal",0)
    temp.write(0)
    PVUtil.releasePV(temp)

    if cal_scb == 1:
        #Disable calibration mode for SCB channel
        temp = PVUtil.createPV(cal_ch +":EnCalibMode",5000)
        temp.write(0)
        PVUtil.releasePV(temp)

    temp = PVUtil.createPV("loc://SCB_meas",5000)
    temp.write(SCBmeas)
    PVUtil.releasePV(temp)
    sleep(2)

    temp = PVUtil.createPV("loc://LLRF_meas",5000)
    temp.write(LLRFmeas)
    PVUtil.releasePV(temp)
    sleep(2)

    temp = PVUtil.createPV("loc://PM_meas",5000)
    temp.write(PWMTmeas)
    PVUtil.releasePV(temp)
    sleep(2)

    if cal_scb == 1:
        temp = PVUtil.createPV("loc://SCB_filename",0)
        temp.write(cal_ch.replace(":","_"))
        PVUtil.releasePV(temp)

    temp = PVUtil.createPV("loc://LLRF_filename",0)
    temp.write(llrf_prefix.replace(":","_"))
    PVUtil.releasePV(temp)
