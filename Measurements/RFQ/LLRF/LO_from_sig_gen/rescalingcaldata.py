import pandas as pd
import math

file_name = "RFQ-010:RFS-DIG-102:AI0.csv"

df = pd.read_csv(file_name,names = ('egu', 'raw'), header = 0)
print(df['egu'])

### Convert from Kilowatts tp watts
##df["egu"] = df["egu"] * 1000
##print(df)

# Convert from watts to dBm
for cnt in range(1, len(df["egu"].values)):
    df["egu"].values[cnt] = (10 * math.log(df["egu"].values[cnt],10)) + 30
print(df)

# Add scaling correction in dB
for cnt in range(1, len(df["egu"].values)):
    df["egu"].values[cnt] = df["egu"].values[cnt] + 0.5373
print(df)

# Convert from dBm to watts
for cnt in range(1, len(df["egu"].values)):
    df["egu"].values[cnt] = pow(10, (df["egu"].values[cnt]-30)/10)
print(df)

### Convert watts to KW
##df["egu"] = df["egu"] / 1000
##print(df)

df.to_csv("./corrected/" + file_name, index = False)
